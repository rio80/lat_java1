/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tes;

import com.sun.istack.internal.Nullable;
import com.sun.org.apache.xpath.internal.functions.Function;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.Scanner;
import java.util.Stack;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author rio senjou
 */
public class Tes {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
//        System.out.println(foo(x,8,18,3,6));
        int[] bil = {56, 29, 67, 84, 35, 35, 46, 39, 64, 24, 98,
            67, 42, 67, 45, 88, 77, 33, 22, 64, 23};
//        Calendar tgl = Calendar.getInstance();
//        int bulan = tgl.get(Calendar.MONTH) + 1;
//        int tahun = tgl.get(Calendar.YEAR);
//        int hari = tgl.get(Calendar.DAY_OF_MONTH);

//        String tanggal=String.valueOf(hari+"/"+bulan+"/"+tahun);
//        System.out.println(tanggal);
//        cekKecilBesar(bil);
//        cekGanjilGenap(bil);
//        urutAngka(bil);
//        dateCompare("15/12/2017", "17/12/2017", "dd/MM/yyyy");
//        cetakSegitiga(7);
//        split("nama saya rio", "\\s+");
//        double angle30 = 30;
//        double angle45 = 45;
//
//        System.out.println("Sin 30 = " + Math.sin(angle30));
//        System.out.println("Cos 30 = " + Math.cos(Math.toRadians(angle30)));
//        System.out.println("Tan 30 = " + Math.tan(Math.toRadians(angle30)));
//        System.out.println("Sin 45 = " + Math.sin(Math.toRadians(angle45)));
//        System.out.println("Cos 45 = " + Math.cos(Math.toRadians(angle45)));
//        System.out.println("Tan 45 = " + Math.tan(Math.toRadians(angle45)));
//
//        int radius = 10;
//
//        System.out.println("Luas lingkaran dgn jari2 : " + radius + " Adalah : " + (Math.PI * Math.pow(radius, 2)));
//
//        String[] buah = {"Mangga", "Jeruk", "Apel", "Sawo", "Rambutan", "Duren"};
//        tesRandom(buah);
//
//
//
//        Cetak out1 = new Cetak();
//        Cetak out2 = new Cetak();
//        Cetak out3 = new Cetak();
//        out1.start();
//        out2.start();
//        out3.start();
//
//
//
//
//        Palindrome plndrome = new Palindrome();
//        String textPlndrome = "mala";
//        if (plndrome.isPalindrome(textPlndrome)) {
//            System.out.print("ya");
//        } else {
//            System.out.print("tidak");
//        }
        System.out.println(String.valueOf(wow(8)));

    }

     public static Integer urutanHari() {

        Date now = Calendar.getInstance().getTime();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(now);
        int resultOrderDay = calendar.get(Calendar.DAY_OF_WEEK);
        return resultOrderDay;
    }
     
     
    public static int wow(int x) {
        if (x < 2) {
            return x;
        } else {
            return wow(x - 2) + 3 * wow(x - 1);
        }
    }

    public static class Palindrome {

        private boolean isPalindrome = true;
        private Stack stack;

        public Palindrome() {
            stack = new Stack();
        }

        public boolean isPalindrome(String string) {

            String karakter = string.replaceAll("\\s", "").replaceAll("\\W", "");
            int position = 0;

            for (int i = 0; i < karakter.length(); i++) {
                stack.push(karakter.substring(i, i + 1));
            }

            while (stack.size() > 0) {

                if (!karakter.substring(position, position + 1).equalsIgnoreCase(stack.pop().toString())) {
                    isPalindrome = false;
                    break;
                }

                position++;

            }

            return isPalindrome;
        }
    }

    static class Cetak extends Thread {

        public void run() {
            try {
                for (int i = 0; i < 100; i++) {
                    System.out.println("Angka : " + i);
                    Thread.sleep(400);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    static void tesRandom(String[] kata) {
        Random random = new Random();
        String result = "";

        for (int i = 0; i < kata.length; i++) {
            for (int j = 0; j < kata.length; j++) {
                if (j < kata.length - 1) {
                    System.out.print(kata[random.nextInt(kata.length)] + ",");
                } else {
                    System.out.print(kata[random.nextInt(kata.length)]);
                }
            }
            System.out.println("");

        }

    }

    static void split(String kalimat, String pattern) {

        String[] kata = kalimat.split(pattern);

        System.out.println("--hasil split--");
        for (int i = 0; i < kata.length; i++) {
            System.out.println(kata[i] + " - kata ke-" + i);
        }
    }

    static void cetakSegitiga(int jumlah) {
        int input = jumlah;
        int loop2 = 1;
        int spasi = 0;
        for (int i = 1; i <= input; i++) {
            loop2 = i + (i - 1); //utk membuat cetak bintang kesamping dgn jml ganjil
            spasi = ((input * 2) - i) - (input - 1); //utk memberikan spasi antar segitiga
            for (int s = 0; s < input; s++) { //looping untuk cetak segitiga brdasarkan jml input
                for (int k = 0; k < spasi; k++) {
                    System.out.print(" "); //cetak spasi sebelah kiri
                }
                for (int j = 0; j < loop2; j++) {
                    System.out.print("*");
                }
                for (int k = 0; k < spasi; k++) {
                    if (i == input) {
                        System.out.print("|");
                    } else {
                        System.out.print(" ");//cetak spasi sebelah kanan
                    }
                }

            }

            System.out.print(loop2);
            System.out.println(""); //agar hasil cetak * terurut vertical/enter tiap looping
        }
    }

    static void dateCompare(String tglPinjam, String tglKembali, String pattern) {
        try {
            SimpleDateFormat date = new SimpleDateFormat(pattern);
            Date TglPinjam = (Date) date.parse(tglPinjam);
            Date TglKembali = (Date) date.parse(tglKembali);
            long telat = Math.abs(TglKembali.getTime() - TglPinjam.getTime());
            if (TglKembali.getTime() > TglPinjam.getTime()) {
                String tampil = String.valueOf(TimeUnit.MILLISECONDS.toDays(telat));
                System.out.println(tampil);
            }

//            String tampil = String.valueOf(telat);
        } catch (Exception e) {
            System.out.println(e.getMessage().toString());
        }
    }

    static void cekKecilBesar(int[] N) {
        int bilBesar = N[0];
        int bilKecil = N[0];
        int len = N.length;
        for (int i = 0; i < len - 1; i++) {
            int cek = N[i];
            if (bilBesar < cek) {
                bilBesar = cek;
            }
            if (bilKecil > cek) {
                bilKecil = cek;
            }
        }
        System.out.println("Bilangan terbesar : " + bilBesar);
        System.out.println("Bilangan terKecil : " + bilKecil);
    }

    static void cekGanjilGenap(int[] N) {
        int temp;
        String ganjil = "", genap = "";
        int len = N.length;
        for (int i = 0; i < len; i++) {
            if (N[i] % 2 == 0) {
                genap += String.valueOf(N[i]);
                genap += ",";
            }

            if (N[i] % 2 == 1) {
                ganjil += String.valueOf(N[i]);
                ganjil += ",";
            }
        }
        System.out.println("Bilangan ganjil : " + ganjil);
        System.out.println("Bilangan genap : " + genap);
    }

    static void urutAngka(int[] N) {
        int temp = 0;
        int len = N.length;
        int ASC[] = new int[len];
        String hasilAsc = "", hasilDesc = "";
        for (int i = 0; i < len; i++) {
            for (int j = 0; j < len - 1; j++) {
                if (N[j] > N[j + 1]) {
                    temp = N[j];
                    N[j] = N[j + 1];
                    N[j + 1] = temp;
                }
            }
        }
        for (int i = 0; i < len; i++) {
            hasilAsc += String.valueOf(N[i]) + ",";
        }
        System.out.println("Angka diurutkan ASC :" + hasilAsc);

        for (int i = 0; i < len; i++) {
            for (int j = 0; j < len - 1; j++) {
                if (N[j] < N[j + 1]) {
                    temp = N[j];
                    N[j] = N[j + 1];
                    N[j + 1] = temp;
                }
            }
        }
        for (int i = 0; i < len; i++) {
            hasilDesc += String.valueOf(N[i]) + ",";
        }
        System.out.println("Angka diurutkan DESC :" + hasilDesc);
//          System.out.println("Angka diurutkan DESC :"+hasil);
    }

}
